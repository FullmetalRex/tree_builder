$(document).ready(function () {
    readTree();
});

$('#deleteModal').on('show.bs.modal', function (event) {
    startTimer();
    let deleteBranchId = $(event.relatedTarget).data('id');
    let delBranch = document.getElementsByName('deleteBranch');
    delBranch[0].setAttribute("id", deleteBranchId);
});

$('#newTree').click(function () {
    $.ajax({
        url: '/addTrees',
        success: function () {
            clear();
            readTree();
        }
    });
});

$('#deleteTree').click(function () {
    $.ajax({
        url: '/clearTrees',
        success: function () {
            clear();
            readTree();
        }
    });
});

function addBranch(id) {
    $.ajax({
        url: '/addBranch',
        data: {
            id: id,
        },
        success: function (response) {
            clear()
            readTree();
        }
    });
}

function readTree() {
    $.ajax({
        url: '/getSort',
        success: function (response) {
            if (response.length > 0) {
                clear();
                printRoots(response);
            }
        }
    });
}

function changeBranch(id) {
    let name = prompt('Rename branch');

    if (name) {
        $.ajax({
            url: '/editTrees',
            type: 'post',
            data: {
                id: id,
                name: name,
            },
            success: function () {
                clear()
                readTree();
            }
        });
    }
}

function deleteBranch(id) {
    console.log(id);
    $.ajax({
        url: '/deleteTrees',
        data: {
            id: id,
        },
        success: function () {
            $('#deleteModal').modal('hide');
            clear()
            readTree();
        }
    });
}

function clear() {
    $('#branchesTree').empty();
}

function printRoots(branches) {
    $('#branchesTree').append(branches);
}

function startTimer() {
    let timer;
    let my_timer = document.getElementById("my_timer");
    let time = my_timer.innerHTML;
    let timerModal = my_timer.innerHTML = '00:00:20';
    let arr = time.split(":");
    let h = arr[0];
    let m = arr[1];
    let s = arr[2];

    $('#deleteModal').on('hidden.bs.modal', function (event) {
        clearTimeout(timer);
        timerModal;
    });

    if (s == 0) {
        if (m == 0) {
            if (h == 0) {
                $('#deleteModal').modal('hide');
                return;
            }
            h--;
            m = 60;
            if (h < 10) h = "0" + h;
        }
        m--;
        if (m < 10) m = "0" + m;
        s = 59;
    } else s--;
    if (s < 10) s = "0" + s;
    document.getElementById("my_timer").innerHTML = h + ":" + m + ":" + s;
    timer = setTimeout(startTimer, 1000);
}