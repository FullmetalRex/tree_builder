<?php

include 'db/dbConnection.php';

class Tree
{
    public $connection;
    public $branches;
    public $tree;

    public function __construct()
    {
        $dbConnect = new dbConnection();
        $this->connection = $dbConnect->connection;
        $this->readTree();
    }

    public function addTree($name, $id = null)
    {
        if ($id) {
            $this->connection->query('INSERT INTO tree(name, parent_id) VALUES ("'.$name.'", '.$id.')');
            return;
        }

        $this->connection->query('INSERT INTO tree(name) VALUES ("'.$name.'")');
    }

    public function editTree($id, $name)
    {
        $this->connection->query('UPDATE tree SET name = "'.$name.'" WHERE id = '.$id);
    }

    public function readTree()
    {
        $result = $this->connection->query('SELECT * FROM tree');

        while($row = mysqli_fetch_array($result)) {
            $this->branches[$row['id']] = $row;
        }
    }

    public function deleteTree($id)
    {
        $this->connection->query('DELETE FROM tree WHERE id =' .$id);
    }

    public function clearTable()
    {
        $this->connection->query('truncate tree');
    }

    public function sortTree()
    {
        $childrenTree = array();

        if($this->branches) {
            foreach ($this->branches as $branch) {
                $childrenTree[$branch['id']] = $branch['parent_id'];
            }

            foreach ($this->branches as $branch) {
                if (!$branch['parent_id']) {
                    $this->buildTree($branch, $childrenTree);
                }
            }
        }

        return $this->tree;
    }

    public function buildTree($branch, $childrenTree)
    {
        $this->fillBranch($branch);

        $ul = false;

        while (true) {
            $key = array_search($branch['id'], $childrenTree);
            if (!$key) {
                if ($ul) {
                    $this->addTeg('</ul>');
                }
                break;
            }

            unset($childrenTree[$key]);

            if (!$ul) {
                $this->addTeg('<ul>');
                $ul = true;
            }

            $this->buildTree($this->branches[$key], $childrenTree);
        }

        $this->addTeg('</li>');
    }

    public function getSortTree()
    {
        echo $this->sortTree();
        exit;
    }

    private function fillBranch($branch)
    {
        $this->addTeg('<li>');
        $this->addTeg('<div class="p-2">');
        $this->addTeg('<span class="p-2">'.$branch['name'].'</span>');
        $this->addTeg('<div class="btn-group" role="group">');
        $this->addTeg('<button class="btn btn-primary btn-sm" id="'. $branch['id']. '" onclick="addBranch(this.id)"><i class="fas fa-folder-plus"></i></button>');
        $this->addTeg('<button class="btn btn-danger btn-sm" id="'. $branch['id']. '" data-id="'. $branch['id']. '" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash"></i></button>');
        $this->addTeg('<button class="btn btn-info btn-sm" id="'. $branch['id']. '" onclick="changeBranch(this.id)"><i class="fas fa-pen-square"></i></button>');
        $this->addTeg('</div>');
        $this->addTeg('</div>');
    }

    private function addTeg($branch)
    {
        $this->tree = $this->tree . $branch;
    }
}