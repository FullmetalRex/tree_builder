<?php

class Request
{
    public static function get($key)
    {
        return $_GET[$key];
    }

    public static function post($key)
    {
        return $_POST[$key];
    }
}