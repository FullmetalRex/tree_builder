<?php


class Route
{
    private $urls = array();
    private $methods = array();

    public function add($url, $method = null)
    {
        $this->urls[] = '/'.trim($url, '/');

        if ($method != null) {
            $this->methods[] = $method;
        }
    }

    public function submit()
    {
        $urlGetParam = isset($_GET['url']) ? '/'.$_GET['url'] : '/';
        foreach ($this->urls as $key => $value) {
            if (preg_match("#^$value$#", $urlGetParam)) {
                if (is_string($this->methods[$key])) {
                    $useMethod = $this->methods[$key];
                    new $useMethod();
                    exit;
                }

                call_user_func($this->methods[$key]);
            }
        }
    }
}
