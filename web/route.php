<?php

include 'Route/Route.php';
include 'Route/Request.php';
include 'Model/Tree.php';

$router = new Route();

$router->add('/', function() {
    $root = new Tree();
});

$router->add('/addTrees', function() {
    $root = new Tree();
    $root->addTree('mainTree');
});

$router->add('/addBranch', function() {
    $root = new Tree();
    $root->addTree('branchTree', Request::get('id'));
    $root->getSortTree();
});

$router->add('/getSort', function() {
    $root = new Tree();
    $root->getSortTree();
});

$router->add('/deleteTrees', function() {
    $root = new Tree();
    $root->deleteTree(Request::get('id'));
    $root->getSortTree();
});

$router->add('/clearTrees', function() {
    $root = new Tree();
    $root->clearTable();
});

$router->add('/editTrees', function() {
    $root = new Tree();
    $root->editTree(Request::post('id'), Request::post('name'));
    $root->getSortTree();
});

$router->submit();